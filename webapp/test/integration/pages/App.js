sap.ui.require([
        "sap/ui/test/Opa5"
    ],
    function(Opa5) {
        "use strict";
        console.log('App.js');
        Opa5.createPageObjects({
          //The implementation of the page object holds the helper functions we just called in our journey.
            onTheAppPage: {
              // actions performable in the WHEN test section
                actions: {
                    iPressTheSayHelloWithDialogButton: function() {
                        return this.waitFor({
                            controlType: "sap.m.Button",
                            success: function(aButtons) {
                                aButtons[0].$().trigger("tap");
                            },
                            errorMessage: "Did not find the helloDialogButton button on the app page"
                        });
                    }
                },
                // assertions checked in the THEN test section
                assertions: {
                    iShouldSeeTheHelloDialog: function() {
                        return this.waitFor({
                            controlType: "sap.m.Dialog",
                            success: function() {
                                // we set the view busy, so we need to query the parent of the app
                                Opa5.assert.ok(true, "The dialog is open");
                            },
                            errorMessage: "Did not find the dialog control"
                        });
                    }
                }
            }
        });
    });

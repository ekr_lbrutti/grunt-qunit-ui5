sap.ui.require([
    "sap/ui/test/opaQunit"
], function() {
    "use strict";

    console.log('navigationJourney');
    QUnit.module("Navigation");
    // The function opaTest is the main aspect for defining integration tests with OPA.
    // Its parameters define a test name and a callback function that gets executed with the following OPA5 helper objects to write meaningful tests that
    // read like a user story.
    opaTest("Should open the hello dialog", function(Given, When, Then) {
        // Arrangements :
        // On the given object we can call arrangement functions like iStartMyAppInAFrame to load our app in a separate iFrame for integration testing
        //iStartMyAppInAFrame is part of OPA API
        Given.iStartMyAppInAFrame(jQuery.sap.getResourcePath("sap/ui/demo/src/test", ".html"));
        //Actions
        // Contains custom actions that we can execute to get the application in a state where we can test the expected behavior.
        // the method i call onTheAppPage must be implemented in test page object
        When.onTheAppPage.iPressTheSayHelloWithDialogButton();
        // Assertions
        // Contains custom assertions that check a specific constellation in the application and the teardown function that removes our iFrame again.
        // the method i call onTheAppPage must be implemented in test page object
        Then.onTheAppPage.iShouldSeeTheHelloDialog().
        and.iTeardownMyAppFrame(); //<- teardown function
    });
});
